<?php

namespace controllers;

use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;
use Twig\Loader\FilesystemLoader;

class Route
{
    private $collector;
    private $viewController;

    private $route;

    public function __construct($route)
    {
        $this->viewController = new ViewsController();

        $this->route = $route;

        $this->collector = new RouteCollector();

        $this->collector->get("/", function(){
            return $this->viewController->getView($this->route);
        });

        $this->collector->get("/contacto", function(){
            return $this->viewController->getView($this->route);
        });

        $this->collector->get("/portfolio", function(){
            return $this->viewController->getView($this->route);
        });

        $this->collector->get("/quienes-somos", function(){
            return $this->viewController->getView($this->route);
        });

        $this->collector->get("/test", function(){
            return $this->viewController->getView("test");
        });



        $dispatcher = new Dispatcher($this->collector->getData());
        $fullUrl = $_SERVER["REQUEST_URI"];
        $method = $_SERVER['REQUEST_METHOD'];
        $cleanUrl = $this->processInput($fullUrl);

        try {
            echo $dispatcher->dispatch($method, $cleanUrl); # Mandar sólo el método y la ruta limpia
        } catch (HttpRouteNotFoundException $e) {
            echo "Error: Ruta no encontrada";
        } catch (HttpMethodNotAllowedException $e) {
            echo "Error: Ruta encontrada pero método no permitido";
        }

    }

    public function processInput($uri)
    {
        return implode('/',
            array_slice(
                explode('/', $uri), 3));
    }




}
