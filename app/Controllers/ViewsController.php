<?php

namespace controllers;

use mysql_xdevapi\Exception;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Loader\FilesystemLoader;

class ViewsController
{
    private static $pathViews = "./views/";

    public function getView($view)
    {
        return $this->Environment($view);
    }

    private function FilesystemLoader()
    {
        return new FilesystemLoader([self::$pathViews]);
    }

    private function Environment($view)
    {
        $twig = new Environment($this->FilesystemLoader(), [
            'cache' => false,
        ]);

        try {

            echo  $twig->render($view.'.twig');

        }catch(LoaderError $loaderError){

            echo  $twig->render('404.twig', ['loaderError' => $loaderError]);
        }

    }

}