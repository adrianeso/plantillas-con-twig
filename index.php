<?php

    use controllers\InitController;

    require "vendor/autoload.php";

    $url= isset($_GET['url']) ? $_GET['url'] : 'home';

    $init = new InitController($url);
